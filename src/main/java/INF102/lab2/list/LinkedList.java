package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}
	
	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
	private Node<T> getNode(int index) {
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException(index);
		Node<T> currentNode = head;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode;
	}

	@Override
	public void add(int index, T element) {
		if (index < 0 || index > size())
			throw new IndexOutOfBoundsException(index);
		//To add a new node the previous node needs to be updated to have a reference to this new node.
		if (isEmpty()) {	//Special case: the list is empty
			head = new Node<>(element);
			n++;
			return;
		}
		if (index == 0) {	//Special case: adding to index = 0
			//Needs to update head-node
			Node<T> newHead = new Node<>(element);
			newHead.next = head;
			head = newHead;
			n++;
			return;
		}
		if (index == size()) {	//Special case: adding to last index; this node does not have a value for 'next'
			Node<T> previousNode = getNode(index - 1);
			Node<T> newNode = new Node<>(element);
			previousNode.next = newNode;
			n++;
			return;
		}
		//General case
		Node<T> previousNode = getNode(index - 1);
		Node<T> newNode = new Node<>(element);
		newNode.next = previousNode.next;
		previousNode.next = newNode;

		//Update size()
		n++;



		//At this point index can be equal to size(); may be problem if using getNode()
		//To add a element at an index, the previous node must be updated to point to this new node
		//	Node<T> nodeBeforeTheAddedNode = getNode(index - 1);	//What if adding to index = 0?
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}