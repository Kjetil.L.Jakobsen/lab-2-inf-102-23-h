package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if (index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}
		
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {
		//Is index oob?
		if (index < 0 || index > size()) {
			throw new IndexOutOfBoundsException();
		}
		//Increase size
		n++;
		//Expand capacity if needed
		if (size() == elements.length) {
			expandCapacity();
		}
		//Copy old element at the index and replace with the new one
		T elementCopy1 = get(index);
		T elementCopy2;
		elements[index] = element;
		//Move all elements to the right
		boolean continueLoop = true;
		while (continueLoop && index + 1 < size()) {
			if (get(index+1) == null)
				continueLoop = false;
			elementCopy2 = get(index + 1);
			if (elementCopy1 != null)
				elements[index + 1] = elementCopy1;
			elementCopy1 = elementCopy2;
			index++;
		}
	}
	
	private void expandCapacity() {
		int newCapacity = elements.length * 2;
		Object[] elementsWithMoreCapacity = new Object[newCapacity];
		for (int i = 0; i < elements.length; i++) {
			elementsWithMoreCapacity[i] = elements[i];
		}
		elements = elementsWithMoreCapacity;
	}

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}