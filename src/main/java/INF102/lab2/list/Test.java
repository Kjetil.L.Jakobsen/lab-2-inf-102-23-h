package INF102.lab2.list;

public class Test {
    
    public static void main(String[] args) {
		
        List<Integer> list = new ArrayList<>();
		System.out.println(list.size());

        Integer nElements = 100;
		list = addNElements(list, nElements);
		int currentSize = list.size();

        System.out.println(list);

		Integer element = 42;
		Integer index = 50;
		list.add(index, element);

        System.out.println(list);

		Integer newSize = list.size();
		

		//assertEquals(currentSize, newSize - 1);
		/* 
		for(int i=0; i<=100; i++) {
			if(i<index)
				assertEquals(i, list.get(i));
			if(i>index)
				assertEquals(i-1, list.get(i));
		}
        */

    }

    public static List<Integer> addNElements(List<Integer> list, int n) {
		for (Integer i = 0; i < n; i++) {
			list.addLast(i);
		}
        return list;
	}
}
